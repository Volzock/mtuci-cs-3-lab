# 6.0001 Problem Set 3
#
# The 6.0001 Word Game
# Created by: Kevin Luu <luuk> and Jenna Wiens <jwiens>
#
# Name          : <your name>
# Collaborators : <your collaborators>
# Time spent    : <total time>

import math
import random
import string

VOWELS = 'aeiou'
CONSONANTS = 'bcdfghjklmnpqrstvwxyz'
HAND_SIZE = 7

SCRABBLE_LETTER_VALUES = {
    'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8, 'k': 5, 'l': 1, 'm': 3, 'n': 1, 'o': 1, 'p': 3, 'q': 10, 'r': 1, 's': 1, 't': 1, 'u': 1, 'v': 4, 'w': 4, 'x': 8, 'y': 4, 'z': 10
}

# -----------------------------------
# Helper code
# (you don't need to understand this helper code)

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.append(line.strip().lower())
    print("  ", len(wordlist), "words loaded.")
    return wordlist


def get_frequency_dict(sequence):
    """
    Returns a dictionary where the keys are elements of the sequence
    and the values are integer counts, for the number of times that
    an element is repeated in the sequence.

    sequence: string or list
    return: dictionary
    """
    
    # freqs: dictionary (element_type -> int)
    freq = {}
    for x in sequence:
        freq[x] = freq.get(x,0) + 1
    return freq
	

# (end of helper code)
# -----------------------------------

#
# Problem #1: Scoring a word
#
def get_word_score(word, n):
    word = word.lower()
    word_score = 7 * len(word) - 3 * (n - len(word))
    return sum(SCRABBLE_LETTER_VALUES.get(char, 0) for char in word) * (word_score if word_score > 1 else 1)


#
# Make sure you understand how this function works and what it does!
#
def display_hand(hand):
    return "".join((k + ' ') * v for k, v in hand.items())


#
# Make sure you understand how this function works and what it does!
# You will need to modify this for Problem #4.
#
def deal_hand(n):
    hand={}
    num_vowels = int(math.ceil(n / 3))

    for i in range(num_vowels - 1):
        x = random.choice(VOWELS)
        hand[x] = hand.get(x, 0) + 1
    hand['*'] = 1
    
    for i in range(num_vowels, n):    
        x = random.choice(CONSONANTS)
        hand[x] = hand.get(x, 0) + 1
    
    return hand


#
# Problem #2: Update a hand by removing letters
#
def update_hand(hand, word):
    new_hand = hand.copy()

    for char in word.lower():
        if char in new_hand.keys():
            new_hand[char] -= 1
            if new_hand[char] < 1:
                new_hand.pop(char)

    return new_hand


#
# Problem #3: Test word validity
#
def valuating_word(word, hand, word_list):
    if word not in word_list:
        return False
    elif len(set(word) & set(hand.keys())) != len(set(word)):
        return False
    else:
        for char in set(word):
            if word.count(char) > hand[char]:
                return False
    return True


def is_valid_word(word, hand, word_list):
    word = word.lower()
    if not word.count("*"):
        return valuating_word(word, hand, word_list)
    else:
        for vowel in VOWELS:
            new_hand = hand.copy()
            new_hand[vowel] = new_hand.get(vowel, 0) + new_hand.pop("*")
            if valuating_word(word.replace("*", vowel).strip(), new_hand, word_list):
                return True
        return False


#
# Problem #5: Playing a hand
#
def calculate_handlen(hand):
    return len("".join(k * v for k,v in hand.items()))


def play_hand(hand, word_list):
    total_score = 0
    while True:
        print(f"Current Hand: {display_hand(hand)}")
        choice = input('Enter word, or "!!" to indicate that you are finished: ')
        if choice == "!!":
            return (total_score, False)
        elif is_valid_word(choice, hand, word_list):
            tmp_score = get_word_score(choice, calculate_handlen(hand))
            hand = update_hand(hand, choice)
            total_score += tmp_score
            print(f'"{choice}" earned {tmp_score} points. Total: {total_score} points')
        else:
            print("That is not a valid word. Please choose another word.")

        if not calculate_handlen(hand):
            return (total_score, True)

        print()


#
# Problem #6: Playing a game
# 


#
# procedure you will use to substitute a letter in a hand
#

def substitute_hand(hand, letter):
    letter = letter.lower()
    if letter not in VOWELS and letter not in CONSONANTS:
        return hand
    new_hand = hand.copy()
    checklist = list(set(VOWELS) ^ (set(VOWELS) & set(hand.keys()))) if letter in VOWELS \
        else list(set(CONSONANTS) ^ (set(CONSONANTS) & set(hand.keys())))
    rand_num = random.randint(0, len(checklist))
    new_hand[checklist[rand_num]] = new_hand.pop(letter)
    return new_hand


def safe_input(output_text, function):
    try:
        return function(input(output_text))
    except:
        print("Please don't break my game :)")
        return safe_input(output_text, type)

    
def play_game(word_list):
    hand_quantity = safe_input("Enter total number of hands: ", int)

    for i in range(hand_quantity):
        while True:
            current_hand = deal_hand(HAND_SIZE)

            print(f"Current hand: {display_hand(current_hand)}")
            if safe_input("Would you like to substitute a letter? ", str).lower().strip() == "yes":
                current_hand = substitute_hand(current_hand, safe_input("Which letter would you like to replace: ", str))

            result = play_hand(current_hand, word_list)
            if result[1]:
                print(f"Ran out of letters. Total score: {result[0]} points")
            else:
                print(f'Total score: {result[0]} points')

            print("----------")

            choice = safe_input("Would you like to replay the hand? ", str)
            if choice.lower() != "yes":
                break


#
# Build data structures used for entire session and play game
# Do not remove the "if __name__ == '__main__':" line - this code is executed
# when the program is run directly, instead of through an import statement
#
if __name__ == '__main__':
    word_list = load_words()
    play_game(word_list)
